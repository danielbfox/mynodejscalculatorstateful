set -x
POD_NAME=$(kubectl get pods -l app=mysql -o jsonpath="{.items[0].metadata.name}")
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "DROP DATABASE IF EXISTS mynodejscalculatorstatefuldb"
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "CREATE DATABASE mynodejscalculatorstatefuldb"
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "USE mynodejscalculatorstatefuldb; CREATE TABLE log (message VARCHAR(255) NOT NULL)"
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "SHOW DATABASES"
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "SHOW TABLES IN mynodejscalculatorstatefuldb"
kubectl exec -it $POD_NAME -- mysql -h mysql -ppassword -e "SHOW COLUMNS IN mynodejscalculatorstatefuldb.log"

