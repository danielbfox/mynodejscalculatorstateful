#!/bin/bash

ZONE=us-east1-b

echo "Deleting cluster"
echo "================"

gcloud container clusters delete mycluster \
	--zone=$ZONE

exit 0


