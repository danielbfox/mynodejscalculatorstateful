#!/bin/bash

ZONE=us-east1-b

echo "Creating the cluster"
echo "--------------------"

gcloud container clusters create mycluster \
	--cluster-version=1.9.7-gke.1 \
	--image-type=COS \
	--machine-type=n1-standard-1 \
	--num-nodes=3 \
	--zone=$ZONE
gcloud container clusters get-credentials mycluster \
	--zone=$ZONE
echo "Cluster nodes (gcloud command) :"
gcloud compute instances list
echo "Cluster nodes (kubectl command) :"
kubectl get nodes

echo "Creation the admin-user service account"
echo "---------------------------------------"

cat > adminuser.yaml <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF

kubectl apply -f adminuser.yaml

exit 0

