var http = require('http');
var url = require('url');
var mysql = require('mysql');
var querystring = require('querystring');
var os = require("os");

// Call back function (called each time a request is sent to the server)
var add = function(req, res) {

     console.log('Received request for URL: ' + req.url);

     res.writeHead(200, {"Content-Type": "text/plain"});

     var page = url.parse(req.url).pathname;
     var sql
     if (page == '/') {
         res.write('Welcome to my nodejs calculator stateful 1.0\n');
         res.write('POD name is ' + os.hostname() +'\n');
         res.write('Request is ' + req);
     } else if (page == '/add') {
         var params = querystring.parse(url.parse(req.url).query);
         if ('num1' in params && 'num2' in params) {
            var n1 = params['num1'];
            var n2 = params['num2'];
            var result = parseInt(n1) + parseInt(n2);
            var resultstr = n1 + ' + ' + n2 + ' = ' + result;
            res.write(resultstr + '\n');
            sql = "INSERT INTO log (message) VALUES ('"+resultstr+"')";
            connection.query(sql, function (err, result) {
                if (err) throw err;
                console.log("1 record inserted : "+resultstr);
            });            
            res.end();
            return;
        } 
    } else if (page == '/list') {
        sql = "SELECT * FROM log";
        var s = 'Content of log table\n';
        console.log(s);
        res.write(s);
        connection.query(sql, function (err, rows, fields) {
            if (err) throw err;
            for (var i in rows) {
                s = 'Row : ' + rows[i].message + '\n';
                console.log(s);
                res.write(s);
            }
            res.end();
        });
        return;
    }
    res.write('Unrecognized URL\n');
    res.write('Use this sample URL to add 10 and 20\n http://<host>:8500/add?num1=10&num2=20\n');
    res.write('Use this sample URL to list the log entries from the db\n http://<host>:8500/list\n');
    res.end();

}

var connection = mysql.createConnection({
  host     : 'mysql',
  user     : 'root',
  password : 'password',
  database : 'mynodejscalculatorstatefuldb'
});
connection.connect(function(err) {
    if (err) throw err;
    console.log('Connected to mynodejscalculatorstatefuldb')
});
var server = http.createServer(add);
server.listen(8500);
